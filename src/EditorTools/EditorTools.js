import React, { Component } from "react";
import "./EditorTools.css";

class EditorTools extends Component {
  render() {
    return (
      <div className="editor-tools">
        <button className="underline" onClick={underline}>
          Underline
        </button>
        <button className="italic" onClick={italic}>
          Italics
        </button>
        <button className="bold" onClick={bold}>
          Bold
        </button>
        <button className="link" onClick={link}>
          Link
        </button>
        <input
          className="img"
          type="file"
          accept="image/*"
          id="file"
          onChange={image}
        />
        <label htmlFor="file" className="img">
          Image
        </label>
      </div>
    );
  }
}

export default EditorTools;

window.addEventListener(
  "load",
  function() {
    //first check if execCommand and contentEditable attribute is supported or not.
    if (
      document.contentEditable !== undefined &&
      document.execCommand !== undefined
    ) {
      alert("HTML5 Document Editing API Is Not Supported");
    } else {
      document.execCommand("styleWithCSS", false, true);
    }
  },
  false
);

//underlines the selected text
function underline() {
  document.execCommand("underline", false, null);
}

//makes the selected text italic
function italic() {
  document.execCommand("italic", false, null);
}

//makes the selected text bold
function bold() {
  document.execCommand("bold", false, null);
}

//makes the selected text as hyperlink
function link() {
  const url = prompt("Enter the URL");
  document.execCommand("createLink", false, url);
}

function image() {
  var file = document.querySelector("input[type=file]").files[0];

  var reader = new FileReader();

  let dataURI;

  reader.addEventListener(
    "load",
    function() {
      dataURI = reader.result;

      const img = document.createElement("img");
      img.src = dataURI;
      document.getElementsByClassName("image")[0].appendChild(img);
    },
    false
  );

  if (file) {
    console.log("s");
    reader.readAsDataURL(file);
  }
}
