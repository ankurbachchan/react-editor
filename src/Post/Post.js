import React, { Component } from "react";
import { connect } from "react-redux";
import EachPost from "./EachPost";
import EditPost from "../EditPost/EditPost";
import "./Post.css";
class Post extends Component {
  render() {
    return (
      <div>
        <h1>Posts</h1>
        {this.props.posts.map(post => (
          <div key={post.id}>
            {post.editing ? (
              <EditPost post={post} key={post.id} />
            ) : (
              <EachPost key={post.id} post={post} />
            )}
          </div>
        ))}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    posts: state
  };
};
export default connect(mapStateToProps)(Post);
