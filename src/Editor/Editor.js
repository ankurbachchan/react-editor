import React, { Component } from "react";
import "./Editor.css";
import { connect } from "react-redux";
class Editor extends Component {
  handlePost = e => {
    e.preventDefault();
    const post = document.getElementsByClassName("edit")[0].innerHTML;
    const data = {
      id: new Date(),
      post,
      editing: false
    };
    this.props.dispatch({
      type: "ADD_POST",
      data
    });
    document.getElementsByClassName("edit")[0].innerHTML =
      "This is the text editor";
  };
  render() {
    return (
      <div>
        <div className="editor">
          <div className="image" />
          <div className="edit" contentEditable="true">
            This is the text editor
          </div>
        </div>
        <div className="editor-btn">
          <button onClick={this.handlePost}>Post</button>
          <button onClick={preview}>Preview</button>
        </div>
      </div>
    );
  }
}
export default connect()(Editor);

function preview() {
  const body = document.body;
  body.textContent = document.getElementsByClassName("edit")[0].innerHTML;
  body.style.whiteSpace = "pre";
  window.print();
  window.location.reload();
}
