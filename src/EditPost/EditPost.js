import React, { Component } from "react";
import "./EditPost.css";
import { connect } from "react-redux";
class EditPost extends Component {
  handleEdit = e => {
    e.preventDefault();
    const newPost = document.getElementsByClassName("update")[0].innerHTML;
    const data = {
      newPost
    };
    this.props.dispatch({ type: "UPDATE", id: this.props.post.id, data: data });
  };
  render() {
    return (
      <div>
        <div className="update" contentEditable="true" />
        <div className="editor-btn">
          <button onClick={this.handleEdit}>Update</button>
        </div>
      </div>
    );
  }
}
export default connect()(EditPost);
