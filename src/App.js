import React, { Component } from "react";
import "./App.css";
import Editor from "./Editor/Editor";
import Post from "./Post/Post";
import EditorTools from "./EditorTools/EditorTools";

class App extends Component {
  render() {
    return (
      <div className="App">
        <EditorTools />
        <Editor />
        <Post />
      </div>
    );
  }
}

export default App;
